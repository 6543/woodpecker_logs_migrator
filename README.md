# woodpecker_logs_migrator

This is the `migrate-logs-to-log_entries` migration of woodpecker v1.0.0 as standalone binary to run next to woodpecker

Just set `WOODPECKER_DATABASE_DRIVER` and `WOODPECKER_DATABASE_DATASOURCE` with the same vars as you have set for the server.

If they are not set for server, use the default values:

```env
WOODPECKER_DATABASE_DRIVER=sqlite3
WOODPECKER_DATABASE_DATASOURCE=/var/lib/woodpecker/woodpecker.sqlite
```

You can pause the migration at any time by send a SIGTERM signal e.g. `kill <PID>`
