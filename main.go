package main

import (
	"os"
	"strconv"
	"time"

	"codeberg.org/6543/woodpecker_logs_migrator/migration"
	"github.com/rs/zerolog/log"
	"xorm.io/xorm"

	// sql driver
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	e, err := xorm.NewEngine(os.Getenv("WOODPECKER_DATABASE_DRIVER"), os.Getenv("WOODPECKER_DATABASE_DATASOURCE"))
	if err != nil {
		log.Fatal().Err(err)
	}

	sleep := os.Getenv("SLEEP")
	sleepSec, _ := strconv.Atoi(sleep)
	sleepTime := time.Second * time.Duration(sleepSec)

	if err := migration.MigrateLogs2LogEntries(e, sleepTime); err != nil {
		log.Error().Err(err)
		os.Exit(1)
	}
}
