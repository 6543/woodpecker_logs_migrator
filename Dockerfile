FROM golang:1.20 AS build

WORKDIR /src
COPY . .
RUN CGO_ENABLED=1 go build -ldflags '-s -w -extldflags "-static"' -tags 'sqlite sqlite_unlock_notify netgo'

FROM scratch
ENV GODEBUG=netdns=go

# copy certs from build image
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
# copy agent binary
COPY --from=build /src/woodpecker_logs_migrator /woodpecker_logs_migrator

ENTRYPOINT ["/woodpecker_logs_migrator"]
